createdoc
=====

Script for quickly create a sphinx documents


## Install

```
$ curl -s https://raw.githubusercontent.com/superalsrk/ShellToolBox/createdoc/install.sh | sh
```


## Usage

```
$ mkdir doc
$ createdoc
```

