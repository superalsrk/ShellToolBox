#!/bin/bash

if [ "$#" -ne 2 ]; then
	echo 'Usage: bash addftpuser.sh YOURNAME YOURPWD'
	exit 1
fi

BASEPATH=/etc/vsftpd/
VSFTPD_CONF=$BASEPATH/vsftpd.conf
VSFTPD_PWD_ORIGIN=$BASEPATH/virtualuser_passwd.txt
VSFTPD_PWD_DB4=$BASEPATH/virtualuser_passwd.db
VSFTPD_PEM_CONF_FOLDER=$BASEPATH/virtualuser_conf


ftpuser=$1
ftppwd=$2



echo $ftpuser >> $VSFTPD_PWD_ORIGIN
echo $ftppwd >> $VSFTPD_PWD_ORIGIN
db_load -T -t hash -f $VSFTPD_PWD_ORIGIN $VSFTPD_PWD_DB4


echo "local_root=/home/data/ftp/" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "write_enable=YES" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "anon_umask=022" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "anon_world_readable_only=NO" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "anon_upload_enable=YES" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "anon_mkdir_write_enable=YES" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser
echo "anon_other_write_enable=YES" >> $VSFTPD_PEM_CONF_FOLDER/$ftpuser


